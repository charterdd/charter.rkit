﻿using Rocket.API;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Charter.RKIT
{
    public class RKITConfiguration : IRocketPluginConfiguration
    {
        [XmlArrayItem(ElementName = "Permission")]
        public List<string> sList = new List<string>();
        public bool removeFromGroup;
        public string leavegroup;
        public void LoadDefaults()
        {
            sList = new List<string>
            {
                "sert",
                "scsniper",
                "heavyolive"
            };
            removeFromGroup = true;
            leavegroup = "rkit";
        }
    }
}
