﻿using Rocket.API;
using Rocket.Core;
using Rocket.Unturned.Chat;
using Rocket.Unturned.Player;
using SDG.Unturned;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.RKIT
{
    class CommandRkit : IRocketCommand
    {
        public string Name
        {
            get
            {
                return "rkit";
            }
        }
        public string Help
        {
            get
            {
                return "/rkit";
            }
        }
        public string Syntax
        {
            get
            {
                return "/rkit";
            }
        }
        public List<string> Aliases
        {
            get
            {
                return new List<string>
                {

                };
            }
        }
        public List<string> Permissions
        {
            get
            {
                return new List<string>
                {
                    "charter.command.rkit"
                };
            }
        }
        public AllowedCaller AllowedCaller
        {
            get
            {
                return AllowedCaller.Player;
            }
        }

        public void Execute(IRocketPlayer caller, string[] msg)
        {
            UnturnedPlayer player = (UnturnedPlayer)caller;
            if (msg.Length != 0)
            {
                UnturnedChat.Say(player, RKIT.Instance.Translate("error_invalid_args"));
                return;
            }
            /*Random rnd = new Random();
            int Index = rnd.Next(RKIT.Instance.Configuration.Instance.sList.Count);
            string permission = RKIT.Instance.Configuration.Instance.sList[Index];*/
            Random random = new Random();
            int index = random.Next(RKIT.Instance.Configuration.Instance.sList.Count);
            string permission = Convert.ToString(RKIT.Instance.Configuration.Instance.sList[index]);
            if (player.HasPermission(permission))
            {
                UnturnedChat.Say(player, RKIT.Instance.Translate("already_has_perm"));
                return;
            }
            if (R.Permissions.HasPermission(player, permission))
            {
                UnturnedChat.Say(player, RKIT.Instance.Translate("already_has_perm"));
                return;
            }
            CommandWindow.input.onInputText("/p add " + player.CSteamID + " " + permission);
            R.Permissions.AddPlayerToGroup(permission, player);
            if (RKIT.Instance.Configuration.Instance.removeFromGroup)
            {
                R.Permissions.RemovePlayerFromGroup(RKIT.Instance.Configuration.Instance.leavegroup, player);
                UnturnedChat.Say(player, RKIT.Instance.Translate("u_removed"));
                R.Permissions.RemovePlayerFromGroup(RKIT.Instance.Configuration.Instance.leavegroup, player);
                CommandWindow.input.onInputText("/p remove " + player.CSteamID + " " + RKIT.Instance.Configuration.Instance.leavegroup);
            }
            UnturnedChat.Say(player, RKIT.Instance.Translate("u_get_perm", new object[]
            {
                permission
            }));
            UnturnedChat.Say(RKIT.Instance.Translate("player_get_perm", new object[]
            {
                player.CharacterName + " ",
                permission
            }));
        }
    }
}
