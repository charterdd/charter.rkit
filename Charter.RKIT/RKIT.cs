﻿using Rocket.API.Collections;
using Rocket.Core.Logging;
using Rocket.Core.Plugins;
using Rocket.Unturned;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Charter.RKIT
{
    public class RKIT : RocketPlugin<RKITConfiguration>
    {
        protected override void Load()
        {
            Console.WriteLine("Thanks for buy this plugin!");
            Console.WriteLine("Благодарим за покупку этого плагина!");
            RKIT.Instance = this;
        }



        protected override void Unload()
        {
            Logger.Log("Charter.AdminHelp was Unloaded!", ConsoleColor.White);
        }
        public override TranslationList DefaultTranslations
        {
            get
            {
                return new TranslationList()
                {
                    {"error_invalid_args","Error code: 1. Invalid args."},
                    {"u_removed","Вы убраны из группы с командой /rkit!"},
                    {"u_get_perm","Вы получили кит: {0} "},
                    {"player_get_perm","Игрок {0} прописал /rkit и получил кит: {1}"}
                };
            }
        }
        public static RKIT Instance;
    }
}
